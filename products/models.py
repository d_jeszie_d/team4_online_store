from django.db.models import (
  DO_NOTHING, CharField, DateTimeField, ForeignKey,
  Model, TextField, ImageField, DecimalField
)


class Category(Model):
    name = CharField(max_length=128)
    description = TextField()

    def __str__(self):
        # return f"{self.id}, {self.name}"
        return f"{self.name}"


class Product(Model):
    name = CharField(max_length=128)
    category = ForeignKey(Category, on_delete=DO_NOTHING)
    description = TextField()
    price = DecimalField(max_digits=8, decimal_places=2)
    created = DateTimeField(auto_now_add=True)
    image = ImageField(upload_to='images/')
    # video = FileField(upload_to='videos/', null=True, blank="True")

    def __str__(self):
        # return f"{self.id}, {self.name}"
        return f"{self.name}"
