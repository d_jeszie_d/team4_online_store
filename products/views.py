from django.shortcuts import render
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView


from . models import Product

from . forms import ProductForm


def products(request):
    category = request.GET.get('category', '')
    query = Product.objects.all()
    if category:
        query = Product.objects.filter(category__name=category)
    return render(
        request, template_name='homepage.html',
        context={'products': query}
    )


class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'product_add.html'
    success_url = '/'


class ProductUpdate(UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'product_update.html'
    success_url = '/'


class ProductListView(ListView):
    model = Product
    template_name = 'product_list.html'


class ProductDetailView(DetailView):
    model = Product
    template_name = 'product_details.html'
