from django import forms
from models import Authority


class SignupForm(forms.ModelForm):

    class Meta:
        model = Authority
        fields = (
            'username', 'email', 'password'
        )
